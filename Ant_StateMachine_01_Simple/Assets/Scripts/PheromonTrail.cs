﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PheromonTrail {

    IList<PheromonSpot> m_myPheromonSpots = new List<PheromonSpot>();
     
    public void AddPheromon(Vector3 position)
    {
        GameObject newSpot = GameObject.Instantiate(AntHill.s_singelton.GetPheromonTemplate());
        newSpot.transform.position = position; 
        m_myPheromonSpots.Add(newSpot.GetComponent<PheromonSpot>());
    }

    public Vector3 GetTrailPostion(int index)
    {
        PheromonSpot currentSpot = m_myPheromonSpots[index];
        return currentSpot.gameObject.transform.position;
    } 

    public int GetLastIndex()
    {
       return m_myPheromonSpots.Count - 1;
    }

    public void IncreasePheromonPower(int index)
    {
        PheromonSpot currentSpot = m_myPheromonSpots[index];
        currentSpot.IncreasePheromonStrength();
    }

    public int GetPowerOfStartOfTrail()
    {
        PheromonSpot firstSpot = m_myPheromonSpots[0];
        return firstSpot.GetPheromonStrength();
    }

    public int GetPowerOfSpot(int index)
    {
        PheromonSpot spot = m_myPheromonSpots[index];
        return spot.GetPheromonStrength();
    }

    public int GetIndexOfSpot(PheromonSpot spot)
    {
        return m_myPheromonSpots.IndexOf(spot);
    }


}
