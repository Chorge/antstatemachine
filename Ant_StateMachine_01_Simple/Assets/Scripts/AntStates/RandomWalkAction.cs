﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Actions/RadomWalk")]
public class RadomWalkAction : FSM_Action
{   
    [SerializeField] Vector3 m_maximumRange = new Vector3(10,5,0);
      
    public override void Act(FSM_StateController controller)
    {
        MoveToRandomPostions(controller);
    }

    private void MoveToRandomPostions(FSM_StateController controller)
    {
        Ant_StateController antControl = controller as Ant_StateController;

        if (antControl.GetRandomPosReached())
        {
            Vector3 randomPos = controller.gameObject.transform.position +
                new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0);

            randomPos.x = Mathf.Clamp(randomPos.x, -m_maximumRange.x, m_maximumRange.x);
            randomPos.y = Mathf.Clamp(randomPos.y, -m_maximumRange.y, m_maximumRange.y);

            antControl.SetRandomPosition(randomPos);

            antControl.SetRandomPosReached(false);

            antControl.DropPheromon();
        }

        controller.gameObject.transform.position =
                            Vector3.MoveTowards(controller.gameObject.transform.position,
                            antControl.GetRandomPosition(),
                            Time.deltaTime * AntHill.s_singelton.GetAntSpeed());

        if (antControl.GetRandomPosition() 
                == controller.gameObject.transform.position)
        {
            antControl.SetRandomPosReached(true);
        }
    }
}