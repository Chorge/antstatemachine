﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Decisions/AlwaysTrue")]
public class Decision_RetrunTrue : FSM_Decision
{

    public override bool Decide(FSM_StateController controller)
    {
        return true; 
    }
}