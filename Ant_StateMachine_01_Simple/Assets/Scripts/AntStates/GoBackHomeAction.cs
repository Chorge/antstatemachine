﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Actions/GoBackHome")]
public class GoBackHomeAction : FSM_Action
{   
    public override void Act(FSM_StateController controller)
    {
        FollowBackYourTrail(controller);
    }

    private void FollowBackYourTrail(FSM_StateController controller)
    {
        Ant_StateController antControl = controller as Ant_StateController;

        Vector3 trailPositon = antControl.GetCurrentTrailPosition(); 

        controller.gameObject.transform.position =
                            Vector3.MoveTowards(controller.gameObject.transform.position,
                            trailPositon,
                            Time.deltaTime * AntHill.s_singelton.GetAntSpeed());

        if (trailPositon == controller.gameObject.transform.position)
        {
            antControl.GoToNextTrailPosition_BackHome();
        }
    }
}