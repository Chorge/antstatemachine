﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Decisions/EndOfTrail")]
public class Decision_EndOfTrail : FSM_Decision
{ 
    public override bool Decide(FSM_StateController controller)
    {
        Ant_StateController antControl = controller as Ant_StateController;
        return antControl.ReachedEndOfTrail();
    }
}