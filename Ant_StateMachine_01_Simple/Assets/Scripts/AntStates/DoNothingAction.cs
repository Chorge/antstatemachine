﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Actions/DoNothing")]
public class DoNothingAction : FSM_Action
{    
    public override void Act(FSM_StateController controller)
    {
        //Dont do anything
    }
    
}