﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Decisions/AntHillNear")]
public class Decision_AntHillNear : FSM_Decision
{ 
    public override bool Decide(FSM_StateController controller)
    {
        return AntHill.s_singelton.IsNearAnthill(controller.transform.position); 
    }
}