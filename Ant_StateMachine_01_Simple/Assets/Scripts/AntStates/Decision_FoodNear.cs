﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Decisions/FoodNear")]
public class Decision_FoodNear : FSM_Decision
{ 
    public override bool Decide(FSM_StateController controller)
    {
        return AntHill.s_singelton.IsNearFood(controller.transform.position); 
    }
}