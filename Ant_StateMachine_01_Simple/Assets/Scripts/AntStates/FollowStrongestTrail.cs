﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/Actions/PickStrongestTrail")]
public class ChooseStrongestTrailAction : FSM_Action
{   
    public override void Act(FSM_StateController controller)
    {
        PickStrongestTrailAction(controller);
    }

    private void PickStrongestTrailAction(FSM_StateController controller)
    {
        Ant_StateController antControl = controller as Ant_StateController;

        antControl.PickStrongestTrailAction();
    }
}