﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;  

public class FSM_StateController : MonoBehaviour
{

    public FSM_State currentState;  
    float stateTimeElapsed;  
      
    void Update()
    {
        Debug.Log("UPDATE_FMS");
        currentState.UpdateState(this);
    }

    void OnDrawGizmos()
    {
        if (currentState != null)
        {
            Gizmos.color = currentState.sceneGizmoColor; 
        }
    }

    public void TransitionToState(FSM_State nextState)
    {
        currentState = nextState;
        OnExitState(); 
    }

    public bool CheckIfCountDownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return (stateTimeElapsed >= duration);
    }

    private void OnExitState()
    {
        stateTimeElapsed = 0;
    }
}