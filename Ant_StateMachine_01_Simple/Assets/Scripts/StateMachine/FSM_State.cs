﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "FSM/State")]
public class FSM_State : ScriptableObject
{

    public FSM_Action[] actions;
    public FSM_Transition[] transitions;
    public Color sceneGizmoColor = Color.grey;

    public void UpdateState(FSM_StateController controller)
    {
        DoActions(controller);
        CheckTransitions(controller);
    }

    private void DoActions(FSM_StateController controller)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            actions[i].Act(controller);
        }
    }

    private void CheckTransitions(FSM_StateController controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSucceeded = transitions[i].decision.Decide(controller);

            if (decisionSucceeded)
            {
                controller.TransitionToState(transitions[i].trueState);
            }
            else
            {
                controller.TransitionToState(transitions[i].falseState);
            }
        }
    }


}