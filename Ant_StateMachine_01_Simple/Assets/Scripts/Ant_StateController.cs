﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ant_StateController : FSM_StateController {

    Vector3 m_randomPosition;

    bool m_randomPosReached = true;

    PheromonTrail m_myCurrentTrail;

    int m_indexInCurrentTrail = 0;
      
    public void DropPheromon()
    {
        if (m_myCurrentTrail == null)
        {
            m_myCurrentTrail = AntHill.s_singelton.CreateNewTrail();
            m_indexInCurrentTrail = 0;

        }
        else
        {
            m_indexInCurrentTrail++;
        }

        m_myCurrentTrail.AddPheromon(gameObject.transform.position);
    }

    public Vector3 GetCurrentTrailPosition()
    { 
       return m_myCurrentTrail.GetTrailPostion(m_indexInCurrentTrail);
    }

    public void GoToNextTrailPosition_BackHome()
    {
        if (m_indexInCurrentTrail == 0) return;

        m_indexInCurrentTrail--;
        
        m_myCurrentTrail.IncreasePheromonPower(m_indexInCurrentTrail);
    }

    public void GoToNextTrailPosition_TowardsFood()
    {
        if (m_indexInCurrentTrail == m_myCurrentTrail.GetLastIndex()) return;

        m_indexInCurrentTrail++; 

        m_myCurrentTrail.IncreasePheromonPower(m_indexInCurrentTrail);
    }

    public void PickStrongestTrailAction()
    {
        m_myCurrentTrail = AntHill.s_singelton.GetStrongestTrail();
        m_indexInCurrentTrail = 0;
    }
     

    public bool ReachedEndOfTrail()
    {
        return (m_indexInCurrentTrail == m_myCurrentTrail.GetLastIndex());     
    }
    
    public Vector3 GetRandomPosition() { return m_randomPosition; }
    public void SetRandomPosition(Vector3 p) { m_randomPosition = p; }

    public bool GetRandomPosReached() { return m_randomPosReached; }
    public void SetRandomPosReached(bool b) { m_randomPosReached = b; }


    

}
