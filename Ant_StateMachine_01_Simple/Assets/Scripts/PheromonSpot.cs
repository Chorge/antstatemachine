﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PheromonSpot : MonoBehaviour {

    SpriteRenderer m_mySpriteRenderer;

    int m_pheromonStrength = 0;
     
    // Use this for initialization
    void Start () {

        m_mySpriteRenderer = GetComponent<SpriteRenderer>(); 

    }

    public int GetPheromonStrength() { return m_pheromonStrength;  } 

    public void IncreasePheromonStrength() {

        m_pheromonStrength += 1;

        Color newColor = Color.white;

        newColor.b -= 0.01f * (float)m_pheromonStrength;
        newColor.g -= 0.01f * (float)m_pheromonStrength;

        m_mySpriteRenderer.color = newColor;
        m_mySpriteRenderer.sortingOrder = 3 + m_pheromonStrength;

    }
}
