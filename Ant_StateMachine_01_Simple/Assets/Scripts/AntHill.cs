﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntHill : MonoBehaviour {

    public static AntHill s_singelton ; 

    List<PheromonTrail> m_allPheromonTrails = new List<PheromonTrail>();

    GameObject[] m_allFoodObjects;


    [SerializeField] GameObject m_PheromonTemplate;
     
    [SerializeField] float m_antSpeed = 1f;

    private void Start()
    {
        s_singelton = this;

        m_allFoodObjects = GameObject.FindGameObjectsWithTag("Food");
    }

    public GameObject GetPheromonTemplate()
    {
        return m_PheromonTemplate;
    }

    public PheromonTrail CreateNewTrail()
    {
        PheromonTrail newTrail = new PheromonTrail();
        m_allPheromonTrails.Add(newTrail);
        return newTrail;
    }

    public float GetAntSpeed() { return m_antSpeed; }

    public bool IsNearFood(Vector3 position)
    {
        foreach (GameObject food in m_allFoodObjects)
        {
            if (Vector3.Distance(position, food.transform.position) < 1f)
            {
                return true;
            }
        }

        return false;
    }

    public bool IsNearAnthill(Vector3 position)
    {  
        return Vector3.Distance(position, gameObject.transform.position) < 1f;
    }

    public PheromonTrail GetStrongestTrail()
    {
        PheromonTrail strongestTrail = null;
        float powerBuffer = -Mathf.Infinity;

        foreach (PheromonTrail trail in m_allPheromonTrails)
        {
            float currentPheromonPower = trail.GetPowerOfStartOfTrail();

            if (currentPheromonPower > powerBuffer)
            {
                powerBuffer = currentPheromonPower;
                strongestTrail = trail;
            }
        }

        return strongestTrail;
    }
    
}
